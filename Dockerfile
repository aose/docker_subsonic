FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive
ENV SUBSONIC_VERSION 6.0
ENV HOME /root

RUN locale-gen ja_JP.UTF-8 && dpkg-reconfigure locales && \
    echo "Asia/Tokyo" > /etc/timezone && dpkg-reconfigure tzdata

RUN apt-get -qq update && \
    apt-get -y install \
    autoconf automake \
    locales lame git \
    software-properties-common  lib32stdc++6 \
    python-software-properties \
    yasm build-essential libtool libmp3lame-dev wget \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 

ENV PATH $HOME/bin:$PATH
ENV PKG_CONFIG_PATH /usr/local/lib/pkgconfig

RUN mkdir ~/ffmpeg_sources && \
    cd ~/ffmpeg_sources && \
    wget -O fdk-aac.tar.gz https://github.com/mstorsjo/fdk-aac/tarball/master && \
    tar xzvf fdk-aac.tar.gz && \
    rm -rf fdk-aac.tar.gz && \
    cd mstorsjo-fdk-aac* && \
    autoreconf -fiv && \
    ./configure --disable-shared && \
    make && \
    make install && \
    make distclean && \
    cd ~/ffmpeg_sources && \
    wget http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 && \
    tar xjvf ffmpeg-snapshot.tar.bz2 && \
    rm ffmpeg-snapshot.tar.bz2 && \
    cd ffmpeg && \
     ./configure \
      --pkg-config-flags="--static" \
      --extra-cflags="-I/root/ffmpeg_build/include" \
      --extra-ldflags="-L/root/ffmpeg_build/lib" \
      --enable-gpl \
      --enable-libfdk-aac \
      --enable-libmp3lame \
      --enable-nonfree && \
    make && \
    make install && \
    make distclean && \
    hash -r
    
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
    && DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:webupd8team/java -y  \
    && apt-get update \
    && apt-get -y install \
    oracle-java8-installer \
    oracle-java8-set-default \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/oracle-jdk8-installer

RUN cd /tmp && \
    wget "http://subsonic.org/download/subsonic-${SUBSONIC_VERSION}.deb" && \
    dpkg -i /tmp/subsonic-${SUBSONIC_VERSION}.deb && rm -f /tmp/*.deb

EXPOSE 4040

RUN useradd ssman 

RUN mkdir /music /podcast /playlist /subsonic \
	&& chown -R ssman:ssman /music /podcast /playlist /subsonic

VOLUME [ "/music" ]
VOLUME [ "/podcast" ]
VOLUME [ "/playlist" ]
VOLUME [ "/subsonic" ]

USER ssman

CMD export LANG=ja_JP.UTF-8 && \
	rm -rf /subsonic/transcode/ffmpeg && \
	rm -rf /subsonic/transcode/lame && \
	ln -s /usr/local/bin/ffmpeg /subsonic/transcode/ffmpeg && \
	ln -s /usr/bin/lame /subsonic/transcode/lame && \
	/usr/bin/subsonic \
	--home=/subsonic \
	--host=0.0.0.0 \
	--port=4040 \
	--max-memory=100 \
	--default-music-folder=/music \
	--default-podcast-folder=/podcast \
	--default-playlist-folder=/playlist \
	&& sleep 5 && tail -f /subsonic/subsonic_sh.log
