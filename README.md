docker_subsonic
====

Docker container for [Subsonic](www.subsonic.org/), transcoding to HE-AAC by.ffmpeg.


## Description

* Build fdk-aac and ffmpeg
* Install Oracle JDK8
* Run Subsonic

## Requirement

I used this environment.

    $ uname -a
    Linux docker-vm 3.19.0-65-generic #73~14.04.1-Ubuntu SMP Wed Jun 29 21:05:22 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux
    $ docker --version
    Docker version 1.12.0, build 8eab29e
    $ docker-compose -v
    docker-compose version 1.8.0-rc2, build c72c966

## Usage

    $ docker-compose up

Open browser.

    http://YOUR_DOCKER_HOST:4040/

After user registration , set transcoding option.

    ffmpeg -i %s -vn -c:a libfdk_aac -profile:a aac_he -ab 256k -ar 44100 -ac 2 -afterburner 1 -movflags +faststart  pipe:0.aac


## Install
First of all, you must install docker-compose if you not installed.

<https://docs.docker.com/compose/install/>

Clone Dockerfile and build it.

    $ git clone https://bitbucket.org/aose/docker_subsonic
    $ docker-compose build

If you already have music collection, you can create a symbolic link. (music directory is read only from container.)
 
    $ ln -s /PATH/TO/MUSIC_DIR music
    

## Author

[i_aose](https://twitter.com/i_aose)

